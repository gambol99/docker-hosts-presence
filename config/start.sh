#!/bin/bash
#
#   Author: Rohith
#   Date: 2015-06-10 11:57:07 +0100 (Wed, 10 Jun 2015)
#
#  vim:ts=2:sw=2:et
#

CONFD=${CONFD:-/opt/bin/confd}
ETCD_HOSTS=${ETCD_HOSTS:-"http://127.0.0.1:4001"}

echo "Starting up confd service"
$CONFD -node=${ETCD_HOSTS} -backend=etcd -watch=true

