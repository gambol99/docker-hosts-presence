#
#   Author: Rohith
#   Date: 2015-06-10 11:51:06 +0100 (Wed, 10 Jun 2015)
#
#  vim:ts=2:sw=2:et
#
FROM gambol99/alpine-base
MAINTAINER Rohith <gambol99@gmail.com>

ADD config/hosts.toml /etc/confd/conf.d/hosts.toml
ADD config/hosts.cfg.tmpl /etc/confd/templates/hosts.cfg.tmpl
ADD config/start.sh /start.sh

RUN chmod +x ./start.sh

ENTRYPOINT [ "/start.sh" ]
